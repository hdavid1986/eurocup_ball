package com.intland.eurocup.service.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.intland.eurocup.model.CouponRedeem;
import com.intland.eurocup.model.Territory;
import com.intland.eurocup.model.User;
import com.intland.eurocup.persistence.CouponRedeemMapper;
import com.intland.eurocup.persistence.TerritoryMapper;
import com.intland.eurocup.persistence.UserMapper;
import com.intland.eurocup.service.CouponService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CouponServiceImplTest {
	
	@Autowired
	private CouponService couponService;
	
	@MockBean(name = "territoryRepository")
	private TerritoryMapper territoryRepository;
	
	@MockBean(name = "couponRepository")
	private CouponRedeemMapper couponRepository;
	
	@MockBean(name = "userRepository")
	private UserMapper userRepository;
	
	@Captor
	ArgumentCaptor<CouponRedeem> redeemCaptor;
	
	@Test
	public void whenConditionsFine_thenWinBall() {
		
		CouponRedeem couponRedeem = createCoupon();
		Territory territory = createTerritory();
		
		when(territoryRepository.findTerritoryById(territory.getId())).thenReturn(territory);
		when(couponRepository.countTodayWinsForTerritory(territory.getId())).thenReturn(1);
		when(couponRepository.countTotalRedeemsForTerritory(territory.getId())).thenReturn(19);
		when(couponRepository.countWinningCouponsForTerritory(territory.getId())).thenReturn(1);
		when(userRepository.findUserByEmail(couponRedeem.getUser().getEmailAddress())).thenReturn(null);
		
		verifyNoInteractions(territoryRepository, couponRepository, userRepository);
		
		Boolean isWinner = couponService.redeemCoupon(couponRedeem);
		
		verifyNoMoreInteractions(territoryRepository, couponRepository, userRepository);
		
		verify(territoryRepository, times(1)).findTerritoryById(couponRedeem.getUser().getTerritoryId());
		verify(couponRepository, times(1)).countTodayWinsForTerritory(couponRedeem.getUser().getTerritoryId());
		verify(couponRepository, times(1)).countTotalRedeemsForTerritory(couponRedeem.getUser().getTerritoryId());
		verify(couponRepository, times(1)).countWinningCouponsForTerritory(couponRedeem.getUser().getTerritoryId());
		verify(couponRepository, times(1)).lockCoupons();
		verify(couponRepository, times(1)).saveCoupon(redeemCaptor.capture());
		verify(userRepository, times(1)).findUserByEmail(couponRedeem.getUser().getEmailAddress());
		verify(userRepository, times(1)).saveUser(couponRedeem.getUser());
		
		assertTrue(isWinner);
		assertTrue(redeemCaptor.getValue().getWinner());
		assertNotNull(redeemCaptor.getValue().getRedeemDate());
		
	}
	
	private CouponRedeem createCoupon() {
		User user = new User();
		user.setId(1L);
		user.setName("Example Name");
		user.setAge(20);
		user.setAddress("Example Address");
		user.setEmailAddress("example@email.com");
		user.setTerritoryId(1L);
		
		CouponRedeem couponRedeem = new CouponRedeem();
		couponRedeem.setCoupon("testcoupon");
		couponRedeem.setId(1L);
		couponRedeem.setUser(user);
		
		return couponRedeem;
	}
	
	private Territory createTerritory() {
		Territory territory = new Territory();
		territory.setId(1L);
		territory.setName("Hungary");
		territory.setMaxBallNumber(100);
		territory.setMaxWinnersPerDay(10);
		territory.setWinFrequency(20);
		return territory;		
	}
	
	
}
