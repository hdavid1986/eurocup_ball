package com.intland.eurocup.service.impl;

import java.time.LocalDate;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.intland.eurocup.model.CouponRedeem;
import com.intland.eurocup.model.Territory;
import com.intland.eurocup.model.User;
import com.intland.eurocup.persistence.CouponRedeemMapper;
import com.intland.eurocup.persistence.TerritoryMapper;
import com.intland.eurocup.persistence.UserMapper;
import com.intland.eurocup.service.CouponService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Validated
@Service
public class CouponServiceImpl implements CouponService {

	@Autowired
	private TerritoryMapper territoryRepository;

	@Autowired
	private UserMapper userRepository;

	@Autowired
	private CouponRedeemMapper couponRepository;

	@Override
	@Transactional(readOnly = false, isolation = Isolation.SERIALIZABLE)
	public boolean redeemCoupon(@Valid CouponRedeem couponRedeem) {

		boolean isWinner = false;
		Territory territory = territoryRepository.findTerritoryById(couponRedeem.getUser().getTerritoryId());

		couponRepository.lockCoupons();

		User user = userRepository.findUserByEmail(couponRedeem.getUser().getEmailAddress());
		if (user == null) {
			user = couponRedeem.getUser();
			userRepository.saveUser(user);
		}

		isWinner = calculateWinResult(territory);
		
		couponRedeem.setWinner(isWinner);
		couponRedeem.setRedeemDate(LocalDate.now());
		
		couponRepository.saveCoupon(couponRedeem);
		log.info("Winresult: {}", isWinner);
		return isWinner;
	}

	@Override
	@Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
	public Page<CouponRedeem> getWinners(LocalDate fromDate, LocalDate toDate, int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		Page<CouponRedeem> page = couponRepository.getAllWinnersForPeriod(fromDate, toDate);		
		return page;
	}
	
	@Override
	@Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
	public Map<Long, Territory> getTerritories() {
		return territoryRepository.getTerritoryNames();
	}

	private boolean calculateWinResult(Territory territory) {
		
		// get user actual day's win count and total redeem count
		int todayWinsCount = couponRepository.countTodayWinsForTerritory(territory.getId());
		int totalRedeemCount = couponRepository.countTotalRedeemsForTerritory(territory.getId());
		int totalWinsCount = couponRepository.countWinningCouponsForTerritory(territory.getId());
		
		// if redeem count + 1 = territory conditions and there are still balls available then winner else loser.
		if ((totalRedeemCount + 1) % territory.getWinFrequency() == 0 
				&& todayWinsCount <= territory.getMaxWinnersPerDay()
				&& totalWinsCount <= territory.getMaxBallNumber()) {
			return true;
		} else {
			return false;
		}
	}

}
