package com.intland.eurocup.service;

import java.time.LocalDate;
import java.util.Map;

import javax.validation.Valid;

import com.github.pagehelper.Page;
import com.intland.eurocup.model.CouponRedeem;
import com.intland.eurocup.model.Territory;

public interface CouponService {

	public boolean redeemCoupon(@Valid CouponRedeem couponRedeem);
	
	public Page<CouponRedeem> getWinners(LocalDate fromDate, LocalDate toDate, int pageNum, int pageSize);
	
	public Map<Long, Territory> getTerritories();
}
