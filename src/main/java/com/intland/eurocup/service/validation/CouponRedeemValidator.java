package com.intland.eurocup.service.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.intland.eurocup.persistence.CouponRedeemMapper;

@Component
public class CouponRedeemValidator implements ConstraintValidator<UniqueCoupon, String> {

	@Autowired
	private CouponRedeemMapper couponRepository;

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {

		return value != null && !couponRepository.isCouponRedeemed(value);
	}
	
	
	
}
