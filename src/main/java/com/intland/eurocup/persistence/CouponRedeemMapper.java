package com.intland.eurocup.persistence;

import java.time.LocalDate;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.github.pagehelper.Page;
import com.intland.eurocup.model.CouponRedeem;
import com.intland.eurocup.model.User;

@Mapper
public interface CouponRedeemMapper extends UserMapper{

	@Insert("INSERT INTO coupon_redeem (user_id, coupon, redeem_time, winner) VALUES (#{user.id}, #{coupon}, #{redeemDate}, #{winner})")
	@Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
	public int saveCoupon(CouponRedeem coupon);
	
	@Select("SELECT EXISTS (SELECT 1 FROM coupon_redeem WHERE coupon = #{couponCode})")
	public boolean isCouponRedeemed(String couponCode);
	
	@Select("SELECT count(*) FROM coupon_redeem AS c JOIN user AS u ON u.id = c.user_id " + 
			"JOIN territory as t ON u.territory_id = t.id " + 
			"WHERE winner = 'true' " + 
			"AND t.id = #{territoryId}")
	public int countWinningCouponsForTerritory(Long territoryId);
	
	@Select("SELECT count(*) FROM coupon_redeem AS c JOIN user AS u ON u.id = c.user_id " + 
			"WHERE winner = 'true' " + 
			"AND u.territory_id = #{territoryId}" +
			"AND c.redeem_time = SYSDATE()")
	public int countTodayWinsForTerritory(Long territoryId);
	
	@Select("SELECT count(*) FROM coupon_redeem AS c JOIN user AS u ON u.id = c.user_id " + 
			"WHERE u.territory_id = #{territoryId}")
	public int countTotalRedeemsForTerritory(Long territoryId);
	
	@Select("SELECT * FROM coupon_redeem FOR UPDATE")
	public List<CouponRedeem> lockCoupons();
	
	@Select("SELECT * "
			+ "FROM coupon_redeem AS c JOIN user AS u ON u.id = c.user_id "
			+ "JOIN territory as t ON u.territory_id = t.id "
			+ "AND winner = 'true' "
			+ "AND c.redeem_time BETWEEN #{fromDate} AND #{toDate} "
			+ "ORDER BY territory_id, id ")
	@Results({
		@Result(property = "user", javaType = User.class, column = "user_id", one = @One(select = "findUserById")),
		@Result(property = "redeemDate", column = "redeem_time")
	})
	public Page<CouponRedeem> getAllWinnersForPeriod(LocalDate fromDate, LocalDate toDate);
	
}