package com.intland.eurocup.persistence;

import java.util.Map;

import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.intland.eurocup.model.Territory;

@Mapper
public interface TerritoryMapper {

	@Select("SELECT * FROM territory WHERE id = #{id}")
	@Results({
		@Result(property = "maxBallNumber", column = "max_balls"),
		@Result(property = "maxWinnersPerDay", column = "max_winners_a_day"),
		@Result(property = "winFrequency", column = "win_frequency")
	})
	public Territory findTerritoryById(Long id);
	
	@Select("SELECT * FROM territory WHERE name = #{name}")
	@Results({
		@Result(property = "maxBallNumber", column = "max_balls"),
		@Result(property = "maxWinnersPerDay", column = "max_winners_a_day"),
		@Result(property = "winFrequency", column = "win_frequency")
	})
	public Territory findTerritoryByName(String name);
	
	@Select("SELECT id, name FROM territory")
	@MapKey("id")
	public Map<Long, Territory> getTerritoryNames();
}
