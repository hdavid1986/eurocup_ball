package com.intland.eurocup.persistence;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.context.annotation.Primary;

import com.intland.eurocup.model.User;

@Mapper
@Primary
public interface UserMapper extends TerritoryMapper{

	@Select("SELECT * FROM user WHERE email_address = #{emailAddress}")
	@Results({
		@Result(property = "emailAddress", column = "email_address"),
		@Result(property = "territoryId", column = "territory_id")
	})
	public User findUserByEmail(String email);
	
	@Select("SELECT * FROM user WHERE id = #{id}")
	@Results({
		@Result(property = "emailAddress", column = "email_address"),
		@Result(property = "territoryId", column = "territory_id")
	})
	public User findUserById(Long id);
	
	@Insert("INSERT INTO user (name, age, address, email_address, territory_id) VALUES (#{name}, #{age}, #{address}, #{emailAddress}, #{territoryId})")
	@Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
	public int saveUser(User user);
	
}
