package com.intland.eurocup.rest.impl;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.Page;
import com.intland.eurocup.model.CouponRedeem;
import com.intland.eurocup.rest.CouponServiceController;
import com.intland.eurocup.service.CouponService;

@RestController
@RequestMapping("/coupon-service/v1/")
public class CouponServiceControllerImpl implements CouponServiceController{

	@Autowired
	private CouponService couponService;
	
	@GetMapping("/winners")
	public Page<CouponRedeem> getWinners( @RequestParam(defaultValue = "2020-01-01") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fromDate
										, @RequestParam(defaultValue = "2100-01-01") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate toDate
										, @RequestParam(defaultValue = "10") int pageSize
										, @RequestParam(defaultValue = "1") int pageNum) {
		
		return couponService.getWinners(fromDate, toDate, pageNum, pageSize);
	}
	
}
