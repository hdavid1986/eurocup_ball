package com.intland.eurocup.rest.dto;

import lombok.Data;

@Data
public class WinningCouponsDto {

	private String couponCode;
	private String emailAddress;
}
