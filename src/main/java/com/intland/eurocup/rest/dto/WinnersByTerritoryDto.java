package com.intland.eurocup.rest.dto;

import java.util.List;

import lombok.Data;

@Data
public class WinnersByTerritoryDto {

	private String territoryName;
	private List<WinningCouponsDto> winners;
}
