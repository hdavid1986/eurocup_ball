package com.intland.eurocup;

import java.time.LocalDate;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.intland.eurocup.model.CouponRedeem;
import com.intland.eurocup.model.User;
import com.intland.eurocup.persistence.CouponRedeemMapper;
import com.intland.eurocup.persistence.TerritoryMapper;
import com.intland.eurocup.persistence.UserMapper;
import com.intland.eurocup.service.CouponService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class EurocupServiceApplication implements CommandLineRunner{
	
	@Autowired
	private UserMapper userRepository;
	
	@Autowired
	private TerritoryMapper territoryRepository;
	
	@Autowired
	private CouponRedeemMapper couponRepository;
	
	@Autowired
	private CouponService redeemer;
	
	public static void main(String[] args) {
		SpringApplication.run(EurocupServiceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		try {
		log.info("User id 1 -> {}", userRepository.findUserByEmail("hdavid1986@gmail.com"));
		
		log.info("territories -> {}", territoryRepository.getTerritoryNames());
		
		User user = new User("Dávid Horváth", 13, "Hajdúszoboszló", "david.horvath@freemail.hu", territoryRepository.findTerritoryByName("Hungary").getId());

		log.info("coupon: thisIsItab -> {}", redeemer.redeemCoupon(new CouponRedeem(user, "thisIsItab")));
		log.info("coupon: thisIsItac -> {}", redeemer.redeemCoupon(new CouponRedeem(user, "thisIsItac")));
		
		log.info("Winning coupons for territory -> {}", couponRepository.countWinningCouponsForTerritory(userRepository.findUserByEmail("hdavid1986@gmail.com").getTerritoryId()));
		
		log.info("Winners 2 of 3 / 1st page -> {}", redeemer.getWinners(LocalDate.now().minusDays(1), LocalDate.now().plusDays(2), 1, 2));
		log.info("Winners 1 of 3 / second page -> {}", redeemer.getWinners(LocalDate.now().minusDays(1), LocalDate.now().plusDays(2), 2, 2));
		
		} catch (ConstraintViolationException e) {
			log.error(e.getMessage());
		}
	}

}
