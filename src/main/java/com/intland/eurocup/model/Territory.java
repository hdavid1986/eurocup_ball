package com.intland.eurocup.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Territory {

	private Long id;
	private String name;
	private Integer maxBallNumber;
	private Integer maxWinnersPerDay;
	private Integer winFrequency;
		
	public Territory(String name, Integer maxBallNumber, Integer maxWinnersPerDay, Integer winFrequency) {
		super();
		this.name = name;
		this.maxBallNumber = maxBallNumber;
		this.maxWinnersPerDay = maxWinnersPerDay;
		this.winFrequency = winFrequency;
	}
	
	
}
