package com.intland.eurocup.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

	private Long id;
	
	@NotEmpty
	private String name;
	
	@NotNull
	@Min(value = 13, message = "User is too young to participate.")
	private Integer age;
	
	@NotEmpty
	private String address;
	
	@NotEmpty
	@Email(message = "Email address is not valid.")
	private String emailAddress;
	
	@NotNull
	private long territoryId;

	public User(String name, Integer age, String address, String emailAddress, long territoryId) {
		super();
		this.name = name;
		this.age = age;
		this.address = address;
		this.emailAddress = emailAddress;
		this.territoryId = territoryId;
	}

}
