package com.intland.eurocup.model;

import java.time.LocalDate;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.intland.eurocup.service.validation.UniqueCoupon;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CouponRedeem {

	private Long id;
	
	@NotNull
	@Valid
	private User user;
	
	@Size(min = 10, max = 10, message = "Coupon code is not valid.")
	@UniqueCoupon
	private String coupon;
	
	private LocalDate redeemDate;
	
	private Boolean winner;
	
	public CouponRedeem(User user, String coupon) {
		super();
		this.user = user;
		this.coupon = coupon;
	}	

}
