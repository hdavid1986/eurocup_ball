CREATE TABLE territory
(
	id identity not null,
	name varchar(255) not null,
	max_balls integer not null,
	max_winners_a_day integer not null,
	win_frequency integer not null,
	primary key(id)
);

CREATE TABLE user
(
	id identity not null,
	name varchar(255) not null,
	age integer not null,
	address varchar(255) not null,
	email_address varchar(255) not null,
	territory_id integer not null,
	primary key(id),
	foreign key(territory_id) references territory(id)
);

CREATE TABLE coupon_redeem
(
	id identity not null,
	user_id integer not null,
	coupon varchar(255) not null,
	redeem_time date,
	winner boolean,
	primary key(id),
	foreign key(user_id) references user(id)
);