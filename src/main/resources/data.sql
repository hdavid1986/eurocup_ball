INSERT INTO territory VALUES(1, 'Germany', 10000, 250, 40);
INSERT INTO territory VALUES(2, 'Hungary', 5000, 100, 80);

INSERT INTO user VALUES(1, 'David Hornyik', 34, '4271 Mikepercs', 'hdavid1986@gmail.com', 2);
INSERT INTO user VALUES(2, 'Dorottya Horvath', 30, '4024 Debrecen', 'hdorottya@gmail.com', 2);

INSERT INTO coupon_redeem VALUES(1, 1, 'asdgd34hhr', sysdate(), true);
INSERT INTO coupon_redeem VALUES(2, 1, 'ashjv3tdhf', sysdate(), false);
INSERT INTO coupon_redeem VALUES(3, 2, 'ashjv3dsfb', sysdate(), false);
INSERT INTO coupon_redeem VALUES(4, 1, 'asdgd34hhb', sysdate(), true);
INSERT INTO coupon_redeem VALUES(5, 1, 'asdgd34hhn', sysdate(), true);